import 'package:best_match/models/match_db.dart';
import 'package:flutter_test/flutter_test.dart';

void main(){
  test('get zodiac test',() async{
    List<Zodiac> zodiac = await Zodiac.fromWebUrl('http://match.test/api/fetch-zodiac');
    expect(zodiac, isNotNull);
  });
}
import 'package:best_match/models/models.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class ButtonSignState extends Equatable {
  const ButtonSignState();
}

class UndefinedButtonSignState extends ButtonSignState {
  @override
  List<Object> get props => [];

  @override
  String toString() => 'UndefinedButtonSignState';
}

class DefinedButtonSignState extends ButtonSignState {
  final Zodiac maleButtonSign;
  final Zodiac femaleButtonSign;

  DefinedButtonSignState(
      {@required this.maleButtonSign, @required this.femaleButtonSign});

  @override
  List<Object> get props => null;

  @override
  String toString() => 'DefinedButtonSignState';
}

import 'dart:async';
import 'package:best_match/blocs/blocs.dart';
import 'package:best_match/models/button_state_enum.dart';
import 'package:best_match/models/models.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import 'button_sign.dart';

class ButtonSignBloc extends Bloc<ButtonSignEvent, ButtonSignState> {
  final ButtonStateBloc buttonStateBloc;
  StreamSubscription matchStreamSubscription;
  ButtonStateEnum currentButton = ButtonStateEnum.male;
  Zodiac maleButton;
  Zodiac femaleButton;

  ButtonSignBloc({@required this.buttonStateBloc}) {
    matchStreamSubscription = buttonStateBloc.state.listen((state) {
      if (state is FemaleButtonActiveState) {
        currentButton = ButtonStateEnum.female;
      } else if (state is MaleButtonActiveState) {
        currentButton = ButtonStateEnum.male;
      }
    });
  }

  @override
  void dispose() {
    matchStreamSubscription.cancel();
    super.dispose();
  }

  @override
  ButtonSignState get initialState => UndefinedButtonSignState();

  @override
  Stream<ButtonSignState> mapEventToState(
    ButtonSignEvent event,
  ) async* {
    if (event is SelectButtonSignEvent) {
      yield* _mapSelectButtonSignEventToState(event);
    } else if (event is WaitingButtonSignEvent) {
      yield* _mapWaitingButtonSignEventToState();
    } else if (event is ClearButtonSignEvent) {
      yield* _mapClearButtonSignEventToState();
    }
  }

  Stream<ButtonSignState> _mapSelectButtonSignEventToState(
      SelectButtonSignEvent event) async* {
    switch (currentButton) {
      case ButtonStateEnum.male:
        maleButton = event.sign;
        break;
      case ButtonStateEnum.female:
        femaleButton = event.sign;
        break;
    }
    yield DefinedButtonSignState(
        maleButtonSign: maleButton, femaleButtonSign: femaleButton);
  }

  Stream<ButtonSignState> _mapWaitingButtonSignEventToState() async* {
    yield UndefinedButtonSignState();
  }

  Stream<ButtonSignState> _mapClearButtonSignEventToState() async* {
    maleButton = null;
    femaleButton = null;
    yield UndefinedButtonSignState();
  }
}

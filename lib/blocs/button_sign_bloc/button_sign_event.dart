import 'package:best_match/models/match_db.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class ButtonSignEvent extends Equatable {
  const ButtonSignEvent();
}

class SelectButtonSignEvent extends ButtonSignEvent {
  final Zodiac sign;

  SelectButtonSignEvent({@required this.sign});

  @override
  List<Object> get props => null;

  @override
  String toString() => 'SelectButtonSignEvent';
}

class WaitingButtonSignEvent extends ButtonSignEvent {
  @override
  List<Object> get props => null;

  @override
  String toString() => 'WaitingButtonSignEvent';
}

class ClearButtonSignEvent extends ButtonSignEvent {
  @override
  List<Object> get props => null;

  @override
  String toString() => 'ClearButtonSignEvent';
}
import 'package:equatable/equatable.dart';

abstract class ButtonStateEvent extends Equatable {
  const ButtonStateEvent();
}

class ChangeToFemaleButtonEvent extends ButtonStateEvent {

  @override
  List<Object> get props => null;

  @override
  String toString() =>
      'ChangeToFemaleButtonEvent';
}

class ChangeToMaleButtonEvent extends ButtonStateEvent {

  @override
  List<Object> get props => null;

  @override
  String toString() =>
      'ChangeToMaleButtonEvent';
}
import 'dart:async';
import 'package:bloc/bloc.dart';
import './button_state.dart';

class ButtonStateBloc extends Bloc<ButtonStateEvent, ButtonStateState> {
  @override
  ButtonStateState get initialState => MaleButtonActiveState();

  @override
  Stream<ButtonStateState> mapEventToState(
    ButtonStateEvent event,
  ) async* {
    if (event is ChangeToFemaleButtonEvent) {
      yield* _mapChangeToFemaleButtonEventToState();
    } else if (event is ChangeToMaleButtonEvent) {
      yield* _mapChangeToMaleButtonEventToState();
    }
  }

  Stream<ButtonStateState> _mapChangeToFemaleButtonEventToState() async* {
    yield FemaleButtonActiveState();
  }

  Stream<ButtonStateState> _mapChangeToMaleButtonEventToState() async* {
    yield MaleButtonActiveState();
  }
}

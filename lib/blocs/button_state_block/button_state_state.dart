import 'package:equatable/equatable.dart';

abstract class ButtonStateState extends Equatable {
  const ButtonStateState();
}

class FemaleButtonActiveState extends ButtonStateState {

  @override
  List<Object> get props => [];

  @override
  String toString() => 'FemaleButtonActiveState';
}

class MaleButtonActiveState extends ButtonStateState {

  @override
  List<Object> get props => null;

  @override
  String toString() => 'MaleButtonActiveState';
}

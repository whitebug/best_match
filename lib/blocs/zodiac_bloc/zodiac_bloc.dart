import 'dart:async';
import 'package:best_match/blocs/blocs.dart';
import 'package:best_match/models/models.dart';
import 'package:bloc/bloc.dart';

class ZodiacBloc extends Bloc<ZodiacEvent, ZodiacState> {
  @override
  ZodiacState get initialState => ZodiacLoadingState();

  @override
  Stream<ZodiacState> mapEventToState(
    ZodiacEvent event,
  ) async* {
    if (event is LoadZodiacEvent) {
      yield* _mapLoadZodiacEventToState();
    } else if (event is LoadZodiacDetailEvent) {
      yield* _mapLoadZodiacDetailEventToState(event);
    }
  }

  Stream<ZodiacState> _mapLoadZodiacEventToState() async* {
    List<Zodiac> zodiac;
    try {
      zodiac = await Zodiac.fromWebUrl('$globalUrl/fetch-zodiac');
      yield ZodiacLoadedState(zodiac: zodiac);
    } catch (e) {
      print('error $e');
    }
  }

  Stream<ZodiacState> _mapLoadZodiacDetailEventToState(
      LoadZodiacDetailEvent event) async* {
    List<Detail> details;
    try {
      details = await Detail.fromWebUrl(
          '$globalUrl/zodiac-details/${event.maleZodiac.name}/${event.femaleZodiac.name}');
      yield DetailLoadedState(detail: details[0]);
    } catch (e) {
      print('error $e');
    }
  }
}

import 'package:best_match/models/models.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

abstract class ZodiacEvent extends Equatable {
  const ZodiacEvent();
}

class LoadZodiacEvent extends ZodiacEvent {
  @override
  List<Object> get props => null;

  @override
  String toString() => 'LoadZodiacEvent';
}

class LoadZodiacDetailEvent extends ZodiacEvent {
  final Zodiac maleZodiac;
  final Zodiac femaleZodiac;

  LoadZodiacDetailEvent(
      {@required this.maleZodiac, @required this.femaleZodiac});

  @override
  List<Object> get props => null;

  @override
  String toString() =>
      'LoadZodiacEvent {male and female zodiacs: ${maleZodiac.name}, ${femaleZodiac.name}';
}

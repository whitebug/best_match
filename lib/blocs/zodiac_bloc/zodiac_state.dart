import 'package:best_match/models/models.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class ZodiacState extends Equatable {
  const ZodiacState();
}

class ZodiacLoadingState extends ZodiacState {
  @override
  List<Object> get props => [];

  @override
  String toString() => 'ZodiacLoadingState';
}

class ZodiacLoadedState extends ZodiacState {
  final List<Zodiac> zodiac;

  ZodiacLoadedState({@required this.zodiac});

  @override
  List<Object> get props => null;

  @override
  String toString() => 'ZodiacLoadedState {first zodiac name: ${zodiac[0].name}';
}

class DetailLoadedState extends ZodiacState {
  final Detail detail;

  DetailLoadedState({@required this.detail});

  @override
  List<Object> get props => null;

  @override
  String toString() => 'DetailLoadedState {detail title: ${detail.title}';
}
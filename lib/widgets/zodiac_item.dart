import 'package:best_match/blocs/blocs.dart';
import 'package:best_match/models/models.dart';
import 'package:best_match/utils/utils.dart';
import 'package:best_match/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ZodiacItem extends StatelessWidget {
  final Zodiac zodiacItem;

  const ZodiacItem({Key key, @required this.zodiacItem}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _buttonSignBloc = BlocProvider.of<ButtonSignBloc>(context);
    return InkWell(
      onTap: () {
        _buttonSignBloc.dispatch(WaitingButtonSignEvent());
        _buttonSignBloc.dispatch(SelectButtonSignEvent(sign: zodiacItem));
      },
      child: Container(
        width: SizeConfig.safeBlockAverage * 10,
        height: SizeConfig.safeBlockAverage * 10,
        child: Center(
          child: BlocBuilder(
            bloc: _buttonSignBloc,
            builder: (BuildContext context, ButtonSignState state) {
              Color backgroundColor = matchIndigo;
              Color textColor = matchZodiacItem;
              Color frameColor = matchZodiacItem;
              if(state is DefinedButtonSignState) {
                if(state.maleButtonSign == zodiacItem) {
                  backgroundColor = matchMaleColor;
                  frameColor = matchMaleColor;
                  textColor = matchMainColor;
                } else if (state.femaleButtonSign == zodiacItem) {
                  backgroundColor = matchButtonTextColor;
                  frameColor = matchButtonTextColor;
                  textColor = matchMainColor;
                }
              }
              return Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  ClipPath(
                    clipper: SignButtonClipper(),
                    child: Container(
                      width: SizeConfig.safeBlockAverage * 12,
                      height: SizeConfig.safeBlockAverage * 12,
                      color: frameColor,
                      child: Center(
                        child: ClipPath(
                          clipper: SignButtonClipper(),
                          child: Container(
                            width: SizeConfig.safeBlockAverage * 11,
                            height: SizeConfig.safeBlockAverage * 11,
                            color: backgroundColor,
                            child: Center(
                              child: Text(
                                zodiacItem.sign,
                                style: TextStyle(
                                  fontFamily: 'Zodiac',
                                  fontSize: SizeConfig.safeBlockAverage * 7,
                                  color: textColor,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Text(
                    zodiacItem.name,
                    style: TextStyle(
                        color: frameColor,
                        fontWeight: FontWeight.bold,
                        fontSize: SizeConfig.safeBlockAverage * 3),
                  ),
                  Text(
                    zodiacItem.dates,
                    style: TextStyle(
                      color: frameColor,
                    ),
                  )
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}

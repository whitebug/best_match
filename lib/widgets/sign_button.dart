import 'package:best_match/blocs/blocs.dart';
import 'package:best_match/models/button_state_enum.dart';
import 'package:best_match/utils/utils.dart';
import 'package:best_match/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SignButton extends StatelessWidget {
  final ButtonStateEnum button;
  final ButtonStateEnum activeButton;

  const SignButton({
    Key key,
    @required this.button,
    @required this.activeButton,
  }) : super(key: key);

  Widget shadow({@required double opacity}) {
    return Opacity(
      opacity: opacity,
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: ClipPath(
          clipper: SignButtonClipper(),
          child: Container(
            height: SizeConfig.safeBlockAverage * 15,
            width: SizeConfig.safeBlockAverage * 15,
            color: matchIconButtonShadow,
          ),
        ),
      ),
    );
  }

  Widget zodiacIcon({@required String sign}) {
    return Text(
      sign,
      style: TextStyle(
        color: Colors.white,
        fontSize: SizeConfig.safeBlockAverage * 10,
        fontFamily: 'Zodiac',
      ),
      textAlign: TextAlign.center,
    );
  }

  Widget questionIcon() {
    return Icon(
      FontAwesomeIcons.question,
      color: Colors.white,
    );
  }

  @override
  Widget build(BuildContext context) {
    final _activeButtonBloc = BlocProvider.of<ButtonStateBloc>(context);
    final _buttonSignBloc = BlocProvider.of<ButtonSignBloc>(context);
    return Opacity(
      opacity: activeButton == button ? 1.0 : 0.7,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          InkWell(
            onTap: () {
              button == ButtonStateEnum.female
                  ? _activeButtonBloc.dispatch(ChangeToFemaleButtonEvent())
                  : _activeButtonBloc.dispatch(ChangeToMaleButtonEvent());
            },
            child: Stack(
              children: <Widget>[
                button == ButtonStateEnum.female
                    ? Positioned(
                        right: SizeConfig.safeBlockAverage * 6,
                        child: shadow(opacity: 0.5),
                      )
                    : Positioned(
                        left: SizeConfig.safeBlockAverage * 6,
                        child: shadow(opacity: 0.5),
                      ),
                button == ButtonStateEnum.female
                    ? Positioned(
                        right: SizeConfig.safeBlockAverage * 4,
                        child: shadow(opacity: 0.6),
                      )
                    : Positioned(
                        left: SizeConfig.safeBlockAverage * 4,
                        child: shadow(opacity: 0.6),
                      ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 25.0, vertical: 15.0),
                  child: ClipPath(
                    clipper: SignButtonClipper(),
                    child: Container(
                      height: SizeConfig.safeBlockAverage * 15,
                      width: SizeConfig.safeBlockAverage * 15,
                      decoration: BoxDecoration(
                        gradient: button == ButtonStateEnum.female
                            ? LinearGradient(
                                colors: [
                                  matchIconButtonShadow,
                                  matchButtonShadowColor
                                ],
                                begin: Alignment.topLeft,
                                end: Alignment.bottomRight,
                              )
                            : LinearGradient(
                                colors: [
                                  matchIconButtonShadow,
                                  matchButtonShadowColor
                                ],
                                begin: Alignment.bottomLeft,
                                end: Alignment.topRight,
                              ),
                      ),
                      child: Container(
                        child: Center(
                          child: BlocBuilder(
                            bloc: _buttonSignBloc,
                            builder:
                                (BuildContext context, ButtonSignState state) {
                              if (state is DefinedButtonSignState) {
                                switch (button) {
                                  case ButtonStateEnum.male:
                                    if(state.maleButtonSign != null) {
                                      return zodiacIcon(sign: state.maleButtonSign.sign);
                                    } else {
                                      return questionIcon();
                                    }
                                    break;
                                  case ButtonStateEnum.female:
                                    if(state.femaleButtonSign != null) {
                                      return zodiacIcon(sign: state.femaleButtonSign.sign);
                                    } else {
                                      return questionIcon();
                                    }
                                    break;
                                  default:
                                    return questionIcon();
                                }
                              } else {
                                return questionIcon();
                              }
                            },
                          ),
                        ),
                        color: Colors.transparent,
                      ),
                    ),
                  ),
                ),
                button == ButtonStateEnum.female
                    ? Positioned(
                        top: SizeConfig.safeBlockAverage * 12,
                        left: SizeConfig.safeBlockAverage * 12,
                        child: Container(
                          width: SizeConfig.safeBlockAverage * 6,
                          height: SizeConfig.safeBlockAverage * 6,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle, color: matchButtonTextColor),
                          child: Icon(
                            FontAwesomeIcons.venus,
                            color: Colors.white,
                          ),
                        ),
                      )
                    : Positioned(
                        top: SizeConfig.safeBlockAverage * 12,
                        right: SizeConfig.safeBlockAverage * 12,
                        child: Container(
                          width: SizeConfig.safeBlockAverage * 6,
                          height: SizeConfig.safeBlockAverage * 6,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: matchMaleColor,
                          ),
                          child: Icon(
                            FontAwesomeIcons.mars,
                            color: Colors.white,
                          ),
                        ),
                      ),
              ],
            ),
          ),
          Container(
            height: SizeConfig.safeBlockVertical * 3,
            color: matchMainColor,
            child: AnimatedOpacity(
              duration: Duration(milliseconds: 10),
              opacity: activeButton == button ? 1 : 0,
              child: Arrow(),
            ),
          )
        ],
      ),
    );
  }
}

import 'package:best_match/utils/utils.dart';
import 'package:best_match/widgets/widgets.dart';
import 'package:flutter/material.dart';

class Arrow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: ArrowClipper(),
      child: Container(
        width: SizeConfig.safeBlockAverage * 5,
        height: SizeConfig.safeBlockAverage * 5,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [matchIndigo, Colors.deepPurpleAccent],
            begin: Alignment.bottomCenter,
            end: Alignment.topCenter,
          ),
        ),
      ),
    );
  }
}
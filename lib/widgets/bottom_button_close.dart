import 'package:best_match/blocs/blocs.dart';
import 'package:best_match/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BottomButtonClose extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _zodiacBloc = BlocProvider.of<ZodiacBloc>(context);
    final _buttonSignBloc = BlocProvider.of<ButtonSignBloc>(context);
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15.0),
      child: Center(
        child: Container(
          width: SizeConfig.safeBlockHorizontal * 60,
          height: SizeConfig.safeBlockAverage * 10,
          child: RaisedButton(
              child: Text(
                'CLOSE',
                style: TextStyle(
                  fontSize: SizeConfig.safeBlockAverage * 4,
                  color: matchButtonTextColor,
                  fontWeight: FontWeight.bold,
                ),
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30.0)),
              onPressed: () {
                _zodiacBloc.dispatch(LoadZodiacEvent());
                _buttonSignBloc.dispatch(ClearButtonSignEvent());
              }),
        ),
      ),
    );
  }
}

import 'package:best_match/blocs/blocs.dart';
import 'package:best_match/models/models.dart';
import 'package:best_match/utils/utils.dart';
import 'package:best_match/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ZodiacList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<Zodiac> zodiac;
    ZodiacBloc _zodiacBloc = BlocProvider.of<ZodiacBloc>(context);
    return BlocBuilder(
      bloc: _zodiacBloc,
      builder: (BuildContext context, ZodiacState state) {
        if (state is ZodiacLoadedState) {
          zodiac = state.zodiac;
        }
        if (state is ZodiacLoadingState) {
          zodiac = [];
        }
        return GridView.builder(
            shrinkWrap: true,
            physics: ClampingScrollPhysics(),
            itemCount: zodiac.length,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: ScreenUtil.ifWideScreen() ? 5 : 3,
              childAspectRatio: 0.9,
            ),
            itemBuilder: (BuildContext context, int index) {
              return ZodiacItem(
                zodiacItem: zodiac[index],
              );
            });
      },
    );
  }
}
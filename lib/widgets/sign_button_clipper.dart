import 'package:flutter/material.dart';

class SignButtonClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    path.lineTo(size.width * 0.3, size.height * 0.1);
    path.quadraticBezierTo(size.width * 0.5, -size.height * 0.1, size.width * 0.7, size.height * 0.1);
    path.lineTo(size.width * 0.9, size.height * 0.3);
    path.quadraticBezierTo(size.width * 1.1, size.height * 0.5, size.width * 0.9, size.height * 0.7);
    path.lineTo(size.width * 0.7, size.height * 0.9);
    path.quadraticBezierTo(size.width * 0.5, size.height * 1.1, size.width * 0.3, size.height * 0.9);
    path.lineTo(size.width * 0.1, size.height * 0.7);
    path.quadraticBezierTo(-size.width * 0.1, size.height * 0.5, size.width * 0.1, size.height * 0.3);
    path.lineTo(size.width * 0.3, size.height * 0.1);
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}

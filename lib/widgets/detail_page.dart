import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:url_launcher/url_launcher.dart';

class DetailPage extends StatelessWidget {
  final String page;

  const DetailPage({Key key, @required this.page}) : super(key: key);

  void _redirectToUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw Exception('Не удалось перейти по адресу $url');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Html(
        data: page,
        useRichText: true,
        defaultTextStyle: new TextStyle(color: Colors.white),
        onLinkTap: (url) => _redirectToUrl(url),
      ),
    );
  }
}

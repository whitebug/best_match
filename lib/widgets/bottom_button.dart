import 'package:best_match/blocs/blocs.dart';
import 'package:best_match/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BottomButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _buttonSignBloc = BlocProvider.of<ButtonSignBloc>(context);
    final _zodiacBloc = BlocProvider.of<ZodiacBloc>(context);
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15.0),
      child: Center(
        child: Container(
          width: SizeConfig.safeBlockHorizontal * 60,
          height: SizeConfig.safeBlockAverage * 10,
          child: BlocBuilder(
            bloc: _buttonSignBloc,
            builder: (BuildContext context, ButtonSignState state) {
              if (state is DefinedButtonSignState) {
                Color textColor = (state.maleButtonSign != null &&
                        state.femaleButtonSign != null)
                    ? Colors.white
                    : matchButtonTextColor;
                return RaisedButton(
                    child: Text(
                      'MATCH NOW',
                      style: TextStyle(
                        fontSize: SizeConfig.safeBlockAverage * 4,
                        color: textColor,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0)),
                    onPressed: (state.maleButtonSign != null &&
                            state.femaleButtonSign != null)
                        ? () {
                            _zodiacBloc.dispatch(LoadZodiacDetailEvent(
                                maleZodiac: state.maleButtonSign,
                                femaleZodiac: state.femaleButtonSign));
                          }
                        : () {});
              } else {
                return RaisedButton(
                    child: Text(
                      'MATCH NOW',
                      style: TextStyle(
                        fontSize: SizeConfig.safeBlockAverage * 4,
                        color: matchButtonTextColor,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0)),
                    onPressed: () {});
              }
            },
          ),
        ),
      ),
    );
  }
}

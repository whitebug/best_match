import 'package:best_match/blocs/blocs.dart';
import 'package:best_match/models/models.dart';
import 'package:best_match/utils/utils.dart';
import 'package:best_match/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Heart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ButtonStateEnum _buttonState;
    final _activeButtonBloc = BlocProvider.of<ButtonStateBloc>(context);
    return Container(
      color: matchMainColor,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 0.0),
            child: Stack(
              children: <Widget>[
                BlocBuilder(
                  bloc: _activeButtonBloc,
                  builder: (BuildContext context, ButtonStateState state) {
                    if(state is MaleButtonActiveState){
                      _buttonState = ButtonStateEnum.male;
                    } else {
                      _buttonState = ButtonStateEnum.female;
                    }
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        SignButton(
                          activeButton: _buttonState,
                          button: ButtonStateEnum.male,
                        ),
                        SignButton(
                          activeButton: _buttonState,
                          button: ButtonStateEnum.female,
                        ),
                      ],
                    );
                  },
                ),
                Positioned(
                  left: SizeConfig.safeBlockHorizontal * 47,
                  top: SizeConfig.safeBlockVertical * 2.0,
                  child: ClipOval(
                    child: Container(
                      width: SizeConfig.safeBlockAverage * 1,
                      height: SizeConfig.safeBlockAverage * 1,
                      color: matchButtonShadowColor,
                    ),
                  ),
                ),
                Positioned(
                  left: SizeConfig.safeBlockHorizontal * 42,
                  top: SizeConfig.safeBlockVertical * 3,
                  child: ClipOval(
                    child: Container(
                      width: SizeConfig.safeBlockAverage * 0.7,
                      height: SizeConfig.safeBlockAverage * 0.7,
                      color: matchButtonShadowColor,
                    ),
                  ),
                ),
                Positioned(
                    left: SizeConfig.safeBlockHorizontal * 45,
                    top: SizeConfig.safeBlockVertical * 4.0,
                    child: ClipOval(
                      child: Container(
                        width: SizeConfig.safeBlockAverage * 0.7,
                        height: SizeConfig.safeBlockAverage * 0.7,
                        color: matchButtonShadowColor,
                      ),
                    )),
                Positioned(
                  left: SizeConfig.safeBlockHorizontal * 47,
                  top: SizeConfig.safeBlockVertical * 11.0,
                  child: ClipOval(
                    child: Container(
                      width: SizeConfig.safeBlockAverage * 1.1,
                      height: SizeConfig.safeBlockAverage * 1.1,
                      color: matchButtonShadowColor,
                    ),
                  ),
                ),
                Positioned(
                  left: SizeConfig.safeBlockHorizontal * 42,
                  top: SizeConfig.safeBlockVertical * 13.0,
                  child: ClipOval(
                    child: Container(
                      width: SizeConfig.safeBlockAverage * 0.7,
                      height: SizeConfig.safeBlockAverage * 0.7,
                      color: matchButtonShadowColor,
                    ),
                  ),
                ),
                Container(
                  height: SizeConfig.safeBlockAverage * 20,
                  child: Center(
                    child: Text(
                      '❤️',
                      style: TextStyle(
                          color: matchButtonShadowColor,
                          fontSize: SizeConfig.safeBlockAverage * 7),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
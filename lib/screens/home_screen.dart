import 'package:best_match/blocs/blocs.dart';
import 'package:best_match/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:best_match/utils/utils.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  ZodiacBloc _zodiacBloc;

  @override
  void initState() {
    super.initState();
    _zodiacBloc = BlocProvider.of<ZodiacBloc>(context);
    _zodiacBloc.dispatch(LoadZodiacEvent());
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Select signs to match'),
        centerTitle: true,
      ),
      body: Container(
        color: matchIndigo,
        child: BlocBuilder(
          bloc: _zodiacBloc,
          builder: (BuildContext context, ZodiacState state) {
            if (state is ZodiacLoadedState) {
              return Column(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Heart(),
                  Expanded(
                    child: ZodiacList(),
                  ),
                  BottomButton(),
                ],
              );
            } else if (state is DetailLoadedState) {
              return Column(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Heart(),
                  Expanded(
                    child: ListView(
                      children: <Widget>[
                        DetailPage(page: state.detail.body,),
                        BottomButtonClose(),
                      ],
                    ),
                  ),

                ],
              );
            } else {
              return Container(
                height: SizeConfig.blockSizeVertical * 100,
                width: SizeConfig.blockSizeHorizontal * 100,
                color: matchMainColor,
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            }
          },
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

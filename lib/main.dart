import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'blocs/blocs.dart';
import 'screens/home_screen.dart';
import 'utils/utils.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  BlocSupervisor.delegate = MatchBlocDelegate();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final ButtonStateBloc _buttonStateBloc = ButtonStateBloc();
  final ZodiacBloc _zodiacBloc = ZodiacBloc();

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: <BlocProvider>[
        BlocProvider<ButtonStateBloc>(
          builder: (BuildContext context) => _buttonStateBloc,
        ),
        BlocProvider<ButtonSignBloc>(
          builder: (BuildContext context) => ButtonSignBloc(
            buttonStateBloc: _buttonStateBloc
          ),
        ),
        BlocProvider<ZodiacBloc>(
          builder: (BuildContext context) => _zodiacBloc,
        ),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: MatchTheme.theme,
        home: HomeScreen(),
      ),
    );
  }
}

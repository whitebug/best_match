import 'package:flutter/material.dart';

const matchIndigo = const Color(0xFF310062);
const matchIconButtonShadow = const Color(0xFF673AB7);
const matchZodiacItem = const Color(0xFF8760cd);
const matchMaleColor = const Color(0xFF40C4FF);
const matchMainColor = const Color(0xFF150d27);
const matchButtonColor = const Color(0xFF880E4F);
const matchButtonTextColor = const Color(0xFFF06292);
const matchButtonShadowColor = const Color(0xFFE91E63);

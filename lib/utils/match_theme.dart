import 'package:best_match/utils/utils.dart';
import 'package:flutter/material.dart';

class MatchTheme {
  static get theme {
    return ThemeData(
        primaryColor: matchMainColor,
        buttonColor: matchButtonColor,
        pageTransitionsTheme: PageTransitionsTheme());
  }
}

import 'package:best_match/utils/size_config.dart';

class ScreenUtil {
  static bool ifWideScreen() {
    if(SizeConfig.blockSizeHorizontal > SizeConfig.blockSizeVertical) {
      return true;
    } else {
      return false;
    }
  }
}
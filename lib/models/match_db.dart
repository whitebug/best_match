import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:sqfentity/sqfentity.dart';
import 'package:sqfentity_gen/sqfentity_gen.dart';

part 'match_db.g.dart';

const globalUrl = 'http://bestmatch.autoleader1.info/api';

const tableZodiac = SqfEntityTable(
  tableName: 'zodiac',
  primaryKeyName: 'id',
  primaryKeyType: PrimaryKeyType.integer_auto_incremental,
  useSoftDeleting: false,
  modelName: null,
  fields: [
    SqfEntityField('sign', DbType.text),
    SqfEntityField('name', DbType.text),
    SqfEntityField('dates', DbType.text),
  ]
);

const tableZodiacDetails = SqfEntityTable(
    tableName: 'details',
    primaryKeyName: 'id',
    primaryKeyType: PrimaryKeyType.integer_auto_incremental,
    useSoftDeleting: false,
    modelName: null,
    fields: [
      SqfEntityField('title', DbType.text),
      SqfEntityField('maleSign', DbType.text),
      SqfEntityField('femaleSign', DbType.text),
      SqfEntityField('body', DbType.text),
    ]
);

@SqfEntityBuilder(matchDbModel)
const matchDbModel = SqfEntityModel(
  modelName: 'MatchDbModel',
  databaseName: 'match.db',
  databaseTables: [tableZodiac,tableZodiacDetails],
  bundledDatabasePath: null,
);